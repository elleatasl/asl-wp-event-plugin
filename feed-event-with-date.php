<?php
/**
 * RSS2 Feed Template for displaying RSS2 Posts feed.
 *
 * @package WordPress
 */

header('Content-Type: ' . feed_content_type('rss2') . '; charset=' . get_option('blog_charset'), true);
$more = 1;

echo '<?xml version="1.0" encoding="'.get_option('blog_charset').'"?'.'>'; ?>

<rss version="2.0"
	xmlns:content="http://purl.org/rss/1.0/modules/content/"
	xmlns:wfw="http://wellformedweb.org/CommentAPI/"
	xmlns:dc="http://purl.org/dc/elements/1.1/"
	xmlns:atom="http://www.w3.org/2005/Atom"
	xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
	xmlns:slash="http://purl.org/rss/1.0/modules/slash/"
	<?php
	/**
	 * Fires at the end of the RSS root to add namespaces.
	 *
	 * @since 2.0.0
	 */
	do_action( 'rss2_ns' );
	?>
>

<channel>
	<title><?php bloginfo_rss('name'); wp_title_rss(); ?></title>
	<atom:link href="<?php self_link(); ?>" rel="self" type="application/rss+xml" />
	<link><?php bloginfo_rss('url') ?></link>
	<description><?php bloginfo_rss("description") ?></description>
	<lastBuildDate><?php echo mysql2date('D, d M Y H:i:s', get_lastpostmodified('GMT'), false); ?></lastBuildDate>
	<language><?php bloginfo_rss( 'language' ); ?></language>
	<?php
	$duration = 'hourly';
	/**
	 * Filter how often to update the RSS feed.
	 *
	 * @since 2.1.0
	 *
	 * @param string $duration The update period.
	 *                         Default 'hourly'. Accepts 'hourly', 'daily', 'weekly', 'monthly', 'yearly'.
	 */
	?>
	<sy:updatePeriod><?php echo apply_filters( 'rss_update_period', $duration ); ?></sy:updatePeriod>
	<?php
	$frequency = '1';
	/**
	 * Filter the RSS update frequency.
	 *
	 * @since 2.1.0
	 *
	 * @param string $frequency An integer passed as a string representing the frequency
	 *                          of RSS updates within the update period. Default '1'.
	 */
	?>
	<sy:updateFrequency><?php echo apply_filters( 'rss_update_frequency', $frequency ); ?></sy:updateFrequency>
	<?php
	/**
	 * Fires at the end of the RSS2 Feed Header.
	 *
	 * @since 2.0.0
	 */
	do_action( 'rss2_head');

    $qobj = get_queried_object();

    $args = array(

        'post_type' => 'spotlight_events',
        'meta_key' => 'event_start',
        'event_type' => $qobj->slug,
        'meta_query' => array(
            array(
                'key' => 'event_start'
            ),
            array(
                'key' => 'event_start_time'
            )
        ),
        'nopaging' => true,
    );

    add_filter( 'posts_orderby', 'asl_edit_event_order' );
	$featured = new WP_Query ( $args );
	remove_filter( 'posts_orderby', 'asl_edit_event_order' );

	while( $featured->have_posts()) : $featured->the_post();
	?>

	<?php
    /* ==================
	 * Date Options
	 */	$allday					= false;
		$multiday				= false;
		$date_start				= get_post_meta( get_the_ID(), 'event_start', true );
		$date_options			= get_post_meta( get_the_ID(), 'scheduling_options', true );
		$start 					= $date_start;
		$end 					= $date_start;

		if ( !$date_options ) {
			$event_start_time = get_post_meta( get_the_ID(), 'event_start_time', true );
			$event_end_time = get_post_meta( get_the_ID(), 'event_end_time', true );
			$start = $date_start . $event_start_time;
			$end 	= $date_start . $event_end_time;

		} else {

			if ( in_array( "allday", $date_options ) ) :

				$allday = true;

			else :

				$end = get_post_meta( get_the_ID(), 'event_end', true ) . get_post_meta( get_the_ID(), 'event_end_time', true );

			endif;

			if ( in_array( "multiday", $date_options ) ) :

				$multiday = true;
				$start = $date_start;
				$end = get_post_meta( get_the_ID(), 'event_end', true );

			endif;

		}
	?>

	<?php if ( strtotime($end) > time() ) : ?>
	<item>
		<title><?php if ( !$allday && !$multiday) { echo date('n/d (g:i a) - ', strtotime( $start ) ); } else { echo 'All Day - '; } the_title_rss() ?></title>
		<link><?php the_permalink_rss() ?></link>
		<comments><?php comments_link_feed(); ?></comments>
		<pubDate><?php echo mysql2date('D, d M Y H:i:s +0000', date('Y-m-d H:i:s', strtotime($start) ), false); ?></pubDate>
		<dc:creator><![CDATA[<?php the_author() ?>]]></dc:creator>
		<?php the_category_rss('rss2') ?>

		<guid isPermaLink="false"><?php the_guid(); ?></guid>
<?php if (get_option('rss_use_excerpt')) : ?>
		<description><![CDATA[<?php the_excerpt_rss(); ?>]]></description>
<?php else : ?>
		<description><![CDATA[<?php the_excerpt_rss(); ?>]]></description>
	<?php $content = get_the_content_feed('rss2'); ?>
	<?php if ( strlen( $content ) > 0 ) : ?>
		<content:encoded><![CDATA[<?php echo $content; ?>]]></content:encoded>
	<?php else : ?>
		<content:encoded><![CDATA[<?php the_excerpt_rss(); ?>]]></content:encoded>
	<?php endif; ?>
<?php endif; ?>
		<wfw:commentRss><?php echo esc_url( get_post_comments_feed_link(null, 'rss2') ); ?></wfw:commentRss>
		<slash:comments><?php echo get_comments_number(); ?></slash:comments>
<?php rss_enclosure(); ?>
	<?php
	/**
	 * Fires at the end of each RSS2 feed item.
	 *
	 * @since 2.0.0
	 */
	do_action( 'rss2_item' );
	?>
	</item>
	<?php endif; ?>
	<?php endwhile; ?>
</channel>
</rss>
