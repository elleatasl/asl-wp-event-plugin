<?php
require_once('library/post-type--databases.php');
require_once('library/post-type--events.php');

add_action( 'after_setup_theme', 'create_upcoming_event_feed' );
function create_upcoming_event_feed() {

    add_feed( 'upcoming', 'render_upcoming_event_feed');

}

add_action( 'after_setup_theme', 'create_upcoming_event_feed_including_date' );
function create_upcoming_event_feed_including_date() {

    add_feed( 'upcoming-with-date', 'render_upcoming_event_feed_including_date');

}

add_action( 'after_setup_theme', 'create_upcoming_event_series_feed_including_date' );
function create_upcoming_event_series_feed_including_date() {

    add_feed( 'series-with-date', 'render_upcoming_event_series_feed_including_date');

}

add_action( 'after_setup_theme', 'create_upcoming_event_series_feed' );
function create_upcoming_event_series_feed() {

    add_feed( 'series-upcoming', 'render_upcoming_event_series_feed');

}

function render_upcoming_event_series_feed() {

    get_template_part( 'feed', 'series-upcoming' );

}



function render_upcoming_event_feed() {

    get_template_part( 'feed', 'event' );

}

function render_upcoming_event_feed_including_date() {

    get_template_part( 'feed', 'event-with-date' );

}

function render_upcoming_event_series_feed_including_date() {

    get_template_part( 'feed', 'series-with-date' );

}




require_once('library/post-type--lists.php');
require_once('library/post-type--taxonomies.php');

/*
3. library/admin.php
    - removing some default WordPress dashboard widgets
    - an example custom dashboard widget
    - adding custom login css
    - changing text in footer of admin
    - prevent users from disabling core plugins
*/
require_once('library/custom-fields.php');
/*
4. library/translation/translation.php
    - adding support for other languages
*/
/*
5. library/bundles/something.php
    - bundles crucial plugins.
*/
//require_once('library/bundles/series/orgSeries.php');
// require_once('library/translation/translation.php'); // this comes turned off by default


/* ==================
 * $URL PARAMETERS
 */ // http://codex.wordpress.org/Function_Reference/get_query_var
function add_query_vars_filter( $vars ) {
    $vars[] = 'for';
    $vars[] = 'search';
    $vars[] = 'json';
    return $vars;
} add_filter( 'query_vars', 'add_query_vars_filter' );


/* ==================
 * $POST-TYPES in RESULTS
 */ // Let's not discriminate ...
function namespace_add_custom_types( $query ) {
  if( is_category() || is_tag() && empty( $query->query_vars['suppress_filters'] ) ) {
    $query->set( 'post_type', array(
     'post', 'spotlight_databases', 'nav_menu_item'
        ));
      return $query;
    }
}
add_filter( 'pre_get_posts', 'namespace_add_custom_types' );

function wpex_fix_shortcodes($content){
    $array = array (
        '<p>[' => '[',
        ']</p>' => ']',
        ']<br />' => ']'
    );

    $content = strtr($content, $array);
    return $content;
}
add_filter('the_content', 'wpex_fix_shortcodes');

/**
 * Sort by custom fields.
 * mt1 refers to meta_1, mt2 to meta_2 and mt3 to meta_3
 *
 * @param $orderby original order by string
 * @return custom order by string
 */
function customorderby($orderby) {
    return 'mt1.meta_value, mt2.meta_value';
}

/**
 * Event Query
 */
function asl_query_events( $query = 'default', $audience = '', $s = null, $taxonomy) {

   switch ( $query ) {

        case 'default' :

            $paged = ( get_query_var( 'paged') ? get_query_var( 'paged' ) : 1 );
            $today = date( 'Ymd' );


            $args = array(

                'post_type' => 'spotlight_events',
                'meta_key' => 'event_start',
                'meta_query' => array(

                    array(

                        'relation' => 'OR',
                        array(
                            'key' => 'event_start',
                            'value' => $today,
                            'compare' => '>='
                        ),
                        array(
                            'key' => 'event_end',
                            'value' => $today,
                            'compare' => '>='
                        )
                    ),

                    array(
                        'key' => 'event_start_time'
                    )
                ),
                'tax_query' => array(
                    array(
                        'taxonomy' => 'series',
                        'field' => 'slug',
                        'terms' => 'staff-development',
                        'operator' => 'NOT IN'
                    )
                ),
                'posts_per_page' => 20,
                'paged' => $paged,
                'library-audience'  => $audience,
                's'                 => ( $s ? $s : null )
            );
            break;

        case 'archive' :

            $qobj = get_queried_object();
            $paged = ( get_query_var( 'paged') ? get_query_var( 'paged' ) : 1 );
            $today = date( 'Ymd' );

            $args = array(

                'post_type' => 'spotlight_events',
                'meta_key' => 'event_start',
                $taxonomy => "$qobj->slug",
                'meta_query' => array(

                    array(

                        'relation' => 'OR',

                        array(
                            'key' => 'event_start',
                            'value' => $today,
                            'compare' => '>='
                        ),

                        array(
                            'key' => 'event_end',
                            'value' => $today,
                            'compare' => '>='
                        )
                    ),

                    array(
                        'key' => 'event_start_time'
                    )

                ),
                'posts_per_page' => 20,
                'paged' => $paged,
                'library-audience'  => $audience

            );

            break;
    }


    add_filter( 'posts_orderby', 'asl_edit_event_order' );
    $the_query = new WP_Query( $args );
    remove_filter( 'posts_orderby', 'asl_edit_event_order' );

    return $the_query;
}

/**
 * Sort by custom fields.
 * mt1 refers to meta_1, mt2 to meta_2 and mt3 to meta_3
 * To be used with the posts_orderby filter. Wrap the event
 * query there.
 *
 * @param $orderby original order by string
 * @return custom order by string
 */
function asl_edit_event_order( $orderby ) {
    return 'mt1.meta_value, mt2.meta_value ASC';
}


/**
 * Change the blog title on ASL WP Theme if events
 */

function unhook_return_asl_wp_theme_site_title() {
    remove_filter( 'asl_wp_theme_site_title_hook', 'return_asl_wp_theme_site_title' );
}
add_action( 'init', 'unhook_return_asl_wp_theme_site_title' );


function return_asl_wp_theme_site_title() {


    if ( is_post_type_archive( 'spotlight_events' ) || get_post_type( get_the_ID() ) === 'spotlight_events' ) :

        echo '<a class="link link--undecorated" href="/sites/spotlight/events/">Programs and Events</a>';

    elseif ( is_post_type_archive( 'list' ) || get_post_type( get_the_ID() ) === 'list' ) :

        echo '<a class="link link--undecorated" href="/sites/spotlight/lists/">Book and Movie Lists</a>';

    elseif ( is_post_type_archive( 'spotlight_databases' ) || get_post_type( get_the_ID() ) === 'spotlight_databases' ) :

        echo '<a class="link link--undecorated" href="/e-library/">E-Library</a> &mdash; <a href="' . get_the_permalink() . '">' . get_the_title() . '</a>';

    endif;

}
add_filter( 'asl_wp_theme_site_title_hook', 'return_asl_wp_theme_site_title', 11, 0 );

/**
 * Deregister admin styles on the front end when using ACF forms
 *
 * ACF makes sure that admin styles are queued when it loads its head, this almost always causes problems with front end forms and isn't needed for our purpose
 */
add_action( 'wp_print_styles', 'custom_acf_deregister_styles', 100 );
function custom_acf_deregister_styles()
{
    if (! is_admin() )
    {
        wp_deregister_style( 'wp-admin' );
    }
}

add_filter( 'manage_edit-spotlight_events_columns', 'my_edit_spotlight_events_columns' ) ;

function my_edit_spotlight_events_columns( $columns ) {

    $columns = array(
        'cb' => '<input type="checkbox" />',
        'title' => __( 'Title <small>( Should be <b>Unique</b> )</small>' ),
        'status' => __( 'Status' ),
        'event_start' => __( 'Start Date' ),
        'event_end' => __( 'End Date' ),
        'series' => __('Series'),
        'date' => __( 'Created' )
    );

    return $columns;
}

add_action( 'manage_spotlight_events_posts_custom_column', 'my_manage_spotlight_events_columns', 10, 2 );

function my_manage_spotlight_events_columns( $column, $post_id ) {
    global $post;

    switch( $column ) {

            /* If displaying the 'duration' column. */
            case 'event_start' :

            /* Get the post meta. */
            $event_start = get_post_meta( $post_id, 'event_start', true );

            /* If no duration is found, output a default message. */
            if ( empty( $event_start ) )
                echo __( '' );

            /* If there is a duration, append 'minutes' to the text string. */
            else
                echo date( 'F j, Y', strtotime( $event_start ));


            break;

            case 'event_end' :

            /* Get the post meta. */
            $event_end = get_post_meta( $post_id, 'event_end', true );

            /* If no duration is found, output a default message. */
            if ( empty( $event_end ) )
                echo __( '' );

            /* If there is a duration, append 'minutes' to the text string. */
            else
                echo date( 'F j, Y', strtotime( $event_end ));


            break;

            case 'series' :

                /* Get the genres for the post. */
                $terms = get_the_terms( $post_id, 'series' );

                /* If terms were found. */
                if ( !empty( $terms ) ) {

                    $out = array();

                    /* Loop through each term, linking to the 'edit posts' page for the specific term. */
                    foreach ( $terms as $term ) {
                        $out[] = sprintf( '<a href="%s">%s</a>',
                            esc_url( add_query_arg( array( 'post_type' => $post->post_type, 'series' => $term->slug ), 'edit.php' ) ),
                            esc_html( sanitize_term_field( 'name', $term->name, $term->term_id, 'series', 'display' ) )
                        );
                    }

                    /* Join the terms, separating them with a comma. */
                    echo join( ', ', $out );
                }

                /* If no terms were found, output a default message. */
                else {
                    _e( 'No Series' );
                }

                break;

       /* Just break out of the switch statement for everything else. */
        default :
            break;
    }
}

add_filter( 'manage_edit-spotlight_events_sortable_columns', 'my_spotlight_events_sortable_columns' );

function my_spotlight_events_sortable_columns( $columns ) {

    $columns['event_start'] = 'event_start';
    $columns['event_end'] = 'event_end';

    return $columns;
}

/* Only run our customization on the 'edit.php' page in the admin. */
add_action( 'load-edit.php', 'my_edit_spotlight_events_load' );

function my_edit_spotlight_events_load() {
    add_filter( 'request', 'my_sort_spotlight_events' );
}

/* Sorts the movies. */
function my_sort_spotlight_events( $vars ) {

    /* Check if we're viewing the 'movie' post type. */
    if ( isset( $vars['post_type'] ) && 'spotlight_events' == $vars['post_type'] ) {

        /* Check if 'orderby' is set to 'duration'. */
        if ( isset( $vars['orderby'] ) && 'event_start' == $vars['orderby'] ) {

            /* Merge the query vars with our custom variables. */
            $vars = array_merge(
                $vars,
                array(
                    'meta_key' => 'event_start',
                    'orderby' => 'meta_value_num'
                )
            );
        }
    }

    return $vars;
}

add_filter( 'acf/update_value/name=asl_event_excerpt', 'update_excerpt_from_short_description', 10, 3 );
function update_excerpt_from_short_description( $value, $post_id, $field ) {

    wp_update_post( array( 'ID' => $post_id, 'post_excerpt' => $value ) );
    return $value;

}

add_filter( 'acf/update_value/name=asl_list_lede', 'update_excerpt_from_list_lede', 10, 3 );
function update_excerpt_from_list_lede( $value, $post_id, $field ) {
    wp_update_post( array( 'ID' => $post_id, 'post_excerpt' => $value ) );
    return $value;
}

add_action( 'acf/save_post', 'add_event_start_time_if_nonexisting', 20 );
function add_event_start_time_if_nonexisting( $post_id ) {

    $value = get_field( 'event_start_time' );

    if ( !$value ) :

        update_field( 'event_start_time', '12:00 am' );

    endif;

}

function set_event_rsvp_cookie() {
    if ( 'POST' == $_SERVER['REQUEST_METHOD'] && !isset( $_COOKIE[ 'patron_has_posted' ] )) {
        setcookie( 'patron_has_posted', 1, time()+20, COOKIEPATH, COOKIE_DOMAIN, false );
    }
}

add_filter('next_posts_link_attributes', 'posts_link_attributes');
add_filter('previous_posts_link_attributes', 'posts_link_attributes');

function posts_link_attributes() {
    return 'class="pagination__link"';
}


	/**
	 * Expose an event's meta fields to the WP REST API (v2)
	 *
	 * @param $post_type
	 * @param $attribute: the name of the field.
	 * @param $args: an array with keys that define the callback functions
	 * @url http://v2.wp-api.org/extending/modifying/
	 */
	 function expose_events_meta_to_api() {

		 register_rest_field( 'spotlight_events', 'asl_event_excerpt', array(

			 'get_callback' => 'return_events_metas_callback'

		 ));

     register_rest_field( 'spotlight_events', 'event_start', array(

			 'get_callback' => 'return_events_metas_callback'

		 ));

     register_rest_field( 'spotlight_events', 'event_start_time', array(

			 'get_callback' => 'return_events_metas_callback'

		 ));

     register_rest_field( 'spotlight_events', 'event_end_time', array(

			 'get_callback' => 'return_events_metas_callback'

		 ));

     register_rest_field( 'spotlight_events', 'event_end', array(

			 'get_callback' => 'return_events_metas_callback'

		 ));


	 }

	 /**
	  * Get the value of the asl_feature_link field
		*
		* @param array $object Details of current post
		* @param string $field_name name of field
		* @param WP_REST_Request $request Current request
		*
		* @return mixed
		*/
	  function return_events_metas_callback( $object, $field_name, $request ) {
		 return get_post_meta( $object[ 'id' ], $field_name, true );
	 }

	/**
	 * Expose the library-audience taxonomy to the REST API
	 */
	 function expose_audience_taxonomy_to_api() {
		global $wp_taxonomies;

		$taxonomy_name = 'library-audience';

		if ( isset( $wp_taxonomies[ $taxonomy_name ] ) ) {
			$wp_taxonomies[ $taxonomy_name ]->show_in_rest = true;
			$wp_taxonomies[ $taxonomy_name ]->rest_base = $taxonomy_name;
			$wp_taxonomies[ $taxonomy_name ]->rest_controller_class = 'WP_REST_Terms_Controller';
		}
	}

	 function create_events_api_endpoint( WP_REST_Request $request ) {

		$audience = $request['audience'];
    $count = $request['count'];
    $series = ( $request['series'] ? $request[ 'series' ] : '' );

		if ( $audience == 'all' ) {
			$audience = array( 'public', 'academic', 'kids', 'teens' );
		}

		$today = date( 'Ymd' );

		$args = array(
			'post_type' => 'spotlight_events',
			'meta_key' 	=> 'event_start',
			'meta_query' => array(

				'relation' => 'OR',
				array(
					'key' => 'event_start',
					'value' => $today,
					'compare' => '>='
				),

				array(
					'key' => 'event_end',
					'value' => $today,
					'compare' => '>='
				)
			),

      array(
        'key' => 'event_start_time'
      ),

			'order' 	=> 'ASC',
			'orderby' => 'meta_value_num',

      'posts_per_page' => ( $count ? $count : 5 ),

			'tax_query' => array(

        'relation' => ( $series ? 'AND' : 'OR'),
				array(
					'taxonomy' => 'library-audience',
					'field' 	=> 'slug',
					'terms' 	=> $audience
				),
        array(
          'taxonomy' => 'series',
          'field'    => 'slug',
          'terms'   => $series
        )
			)
		);

    // Get or set a transient
    if ( false === ( $posts = get_transient( 'asl_event_' . $audience . '_' . $series . '_' . $count ) ) ) { // asl_event_public_5_series
  	  $posts = get_posts( $args );

      // cache for 2 hours
      set_transient( 'asl_event_' . $audience . '_' . $series . '_' . $count, $posts, 60*60*2 );
    }
		$return = array();

		foreach( $posts as $post ){
			$return[] = array(

				'title' => $post->post_title,
				'start' => date( 'F jS', strtotime( get_post_meta( $post->ID, 'event_start', true ) )),
        'from' => get_post_meta( $post->ID, 'event_start_time', true ),
				'end' => ( get_post_meta( $post->ID, 'event_end', true ) ? date( 'F jS', strtotime( get_post_meta( $post->ID, 'event_end', true ) ) ) : null),
        'series' => ( get_the_term_list( $post->ID, 'series' ) ? get_the_term_list( $post->ID, 'series', '| <span itemprop="superEvent">', ', ', '</span>' ) : null ),
        'until' => get_post_meta( $post->ID, 'event_end_time', true ),
        'url' => get_permalink( $post->ID ),
				'excerpt' => $post->post_excerpt,
        'audience' => ( has_term( 'kids', 'library-audience', $post->ID ) ? 'kids' : ( has_term( 'teens', 'library-audience', $post->ID ) ? 'teens' : null ) )

			);
		}

		if ( empty ( $posts ) ) {
			return null;
		}

		$response = new WP_REST_Response( $return );
		return $response;

	}

	function create_events_api_route() {
		register_rest_route( 'events/v2', '/upcoming', array(
			'methods' => 'GET',
			'callback' => 'create_events_api_endpoint'
		));
	}

  /**
	 * Expose an event's meta fields to the WP REST API (v2)
	 *
	 * @param $post_type
	 * @param $attribute: the name of the field.
	 * @param $args: an array with keys that define the callback functions
	 * @url http://v2.wp-api.org/extending/modifying/
	 */
	 function expose_list_meta_to_api() {

     register_rest_field( 'list', 'asl_list_items', array(

			 'get_callback' => 'return_list_metas_callback'

		 ));

	 }

	 /**
	  * Get the value of the asl_feature_link field
		*
		* @param array $object Details of current post
		* @param string $field_name name of field
		* @param WP_REST_Request $request Current request
		*
		* @return mixed
		*/
	  function return_list_metas_callback( $object, $field_name, $request ) {
		 return get_post_meta( $object[ 'id' ], $field_name, true );
	 }


  function create_list_api_endpoint( WP_REST_Request $request ) {

   $audience = $request['audience'];
   $count = $request['count'];
   $name = $request['slug'];

   if ( $audience == 'all' ) {
     $audience = array( 'public', 'academic', 'kids', 'teens' );
   }

   $args = array(
     'post_type' => 'list',
     'name' => $name
   );

   // Get or set a transient
   if ( false === ( $posts = get_transient( 'asl_list' ) ) ) { // asl_event_public_5_series
 	  $posts = get_posts( $args );

     // cache for 2 hours
     set_transient( 'asl_list', $posts, 60*60*2 );
   }

   foreach( $posts as $post ){

     $return[] = array(

       'title' => $post->post_title,
       'url' => get_permalink( $post->ID ),
       'excerpt' => strip_tags( $post->post_excerpt ),
       'media' => ( has_post_thumbnail( $post->ID ) ? wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large', false ) : null )

     );

   }

   if ( empty ( $posts ) ) {
     return null;
   }

   $response = new WP_REST_Response( $return );
   return $response;

 }

 	function create_list_api_route() {
 		register_rest_route( 'lists/v2', '/list', array(
 			'methods' => 'GET',
 			'callback' => 'create_list_api_endpoint'
 		));
 	}

  add_action( 'rest_api_init', function() {

  	remove_filter( 'rest_pre_serve_request', 'rest_send_cors_headers' );
  	add_filter( 'rest_pre_serve_request', function( $value ) {

  		$origin = get_http_origin();
  		if ( $origin && in_array( $origin, array(
  				//define some origins!
          'http://public.library.nova.edu',
          'http://sherman.library.nova.edu',
          'https://sherman.library.nova.edu',
          'http://systemsdev.library.nova.edu',
          'https://systemsdev.library.nova.edu'
  			) ) ) {
  			header( 'Access-Control-Allow-Origin: ' . esc_url_raw( $origin ) );
  			header( 'Access-Control-Allow-Methods: POST, GET, OPTIONS, PUT, DELETE' );
  			header( 'Access-Control-Allow-Credentials: true' );
  		}

  		return $value;

  	});
  }, 15 );

  add_action( 'rest_api_init', 'expose_list_meta_to_api' );
  add_action( 'rest_api_init', 'create_list_api_route' );
  add_action( 'rest_api_init', 'expose_events_meta_to_api' );
  add_action( 'rest_api_init', 'create_events_api_route' );
  add_action( 'init', 'expose_audience_taxonomy_to_api' );
