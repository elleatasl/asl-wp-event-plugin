<?php
if ( 'POST' == $_SERVER['REQUEST_METHOD'] &&
      ! empty($_POST['post_id']) &&
      ! empty($_POST['first_name']) &&
      ! empty($_POST['last_name']) &&
      ! empty($_POST['email_address'])
      && isset($_POST['update_post_nonce']) ) :

  $post_id   = esc_sql( $_POST['post_id'] );
  if ( wp_verify_nonce( $_POST['update_post_nonce'], 'update_post_'. $post_id ) ) {

      $valid = true;

      $field_key  = 'asl_rsvp_registered_person';

      $first_name = esc_sql( $_POST['first_name'] );
      $last_name  = esc_sql( $_POST['last_name'] );

      $email_address = esc_sql( $_POST['email_address'] );
      if ( is_email( $email_address ) ) {
          $valid = false;
      }

      if ( isset( $_POST['phone_number'] ) ) {
          $phone_number = esc_sql( $_POST['phone_number'] );
          if ( !preg_match( "/^(\d[\s-]?)?[\(\[\s-]{0,2}?\d{3}[\)\]\s-]{0,2}?\d{3}[\s-]?\d{4}$/i", $phone_number ) ) {
              $valid = false;
          }
      } else {
          $phone_number = '';
      }

      if ( isset( $_POST['zip'] ) ) {
          $zip = esc_sql( $_POST['zip'] );
      } else {
          $zip = '';
      }

      if ( isset( $_POST['age'] ) ) {
          $age = esc_sql( $_POST[ 'age'] );
          if ( !is_numeric( $age ) ) {
              $valid = false;
          }
      } else {
          $age = '';
      }

      if ( isset( $_POST['library_card'] ) ) {
          $library_card = esc_sql( $_POST[ 'library_card'] );
          if ( !ctype_alnum( $library_card ) ) {
              $valid = false;
          }
      } else {
          $library_card = '';
      }

      if ( isset( $_POST['shark_id'] ) ) {
          $shark_id = esc_sql( $_POST[ 'shark_id'] );
          if ( !ctype_alnum( $shark_id ) ) {
              $valid = false;
          }
      } else {
          $shark_id = '';
      }

      $value = get_field( $field_key, $post_id );
      $value[] = array(
          'first_name' => $first_name,
          'last_name' => $last_name,
          'email_address' => $email_address,
          'phone_number' => $phone_number,
          'zip' => $zip,
          'age' => $age,
          'library_card' => $library_card,
          'shark_id' => $shark_id
      );

      if ( $valid = true ) {

          update_field( $field_key, $value, $post_id );

          $asl_rsvp_email_notifications = ( get_post_meta( get_the_ID(), 'asl_rsvp_email_notifications', true ) );

          $message = 'Hi there, <br><br>';
          $message .= 'Thanks for signing up for ';
          $message .= '<b>' . get_the_title() . '</b> ( ' . get_the_permalink() . ' ). ';
          $message .= "This registration is for <b>" . $first_name . ' ' . $last_name . "</b>. ";
          $message .= "We can't wait to see you! If you have any questions, don't hesitate to contact us.<br><br>";
          $message .= "See you then!";
          $staff_message = "Hi. This is just to tell you that <b>" . $first_name . ' ' . $last_name . '</b> has registered for ' . get_the_title() . '(' . get_the_permalink() . '). ';
          $staff_message .= "We've sent them an email receipt with instructions to contact us with any questions. Remember you can sign in to the event at any time to see all the details or export a spreadsheet.";


          wp_mail( $email_address, 'Event Registration Receipt', $message, array( 'Content-Type: text/html; charset=UTF-8', 'From: Alvin Sherman Library <' . $asl_rsvp_email_notifications . '>' ) );
          wp_mail( $asl_rsvp_email_notifications, 'Event Registration Update', $staff_message, array( 'Content-Type: text/html; charset=UTF-8', 'From: Alvin Sherman Library <libweb@nova.edu>' ) );
          set_event_rsvp_cookie();
          wp_redirect( get_permalink() );

      }

      exit;
  }

endif;
?>
<?php get_header(); ?>

<style type="text/css">
    header td {
        border: none;
    }

    header tr {
        border-bottom: 1px solid white;
    }

    header tr:last-child {
        border-bottom: none;
    }

    header tbody tr:nth-of-type(even) {
        background-color: rgba(255, 255, 255, 0.1);
    }

    .title {
        margin-bottom: 1em !important;
    }


</style>

    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <?php

    // Date options
    $allday                 = false;
    $multiday               = false;
    $date_start             = get_post_meta( get_the_ID(), 'event_start', true );
    $date_options           = get_post_meta( get_the_ID(), 'scheduling_options', true );
    $start                  = $date_start;
    $end                    = $date_start;

    if ( !$date_options ) {
        $event_start_time = get_post_meta( get_the_ID(), 'event_start_time', true );
        $event_end_time = get_post_meta( get_the_ID(), 'event_end_time', true );
        $start = $date_start . $event_start_time;
        $end    = $date_start . $event_end_time;
    } else {

        if ( in_array( "allday", $date_options ) ) :

            $allday = true;

        else :

            $end = get_post_meta( get_the_ID(), 'event_end', true ) . get_post_meta( get_the_ID(), 'event_end_time', true );

        endif;

        if ( in_array( "multiday", $date_options ) ) :

            $multiday = true;
            $start = $date_start;
            $end = get_post_meta( get_the_ID(), 'event_end', true );

        endif;

    }

        $location = get_term_by( 'id', get_post_meta( get_the_ID(), 'location', true), 'location' );

        $contact_info               = ( get_post_meta( get_the_ID(), 'contact_person_options', true) === "yes" ?  true :  false );
        $contact_info__staffMember  = ( $contact_info ? get_user_meta( get_post_meta( get_the_ID(), 'library_staff_member', true) ):  false );
        $contact_info__userdata     = get_userdata( get_post_meta( get_the_ID(), 'library_staff_member', true) );

        $call_to_action             = ( get_post_meta( get_the_ID(), 'has_c2a', true) === "yes" ?  get_post_meta( get_the_ID(), 'c2a_text', true ) :  false );
        $call_to_action_link        = ( get_post_meta( get_the_ID(), 'has_c2a', true )  === 'yes' ? get_post_meta(get_the_ID(), 'c2a_link', true ) : false );

        /**
         * If this event is open for registration
         */
        $asl_rsvp = ( get_post_meta( get_the_ID(), 'asl_rsvp_registration', true ) === 'yes' ? true : false );
        $asl_rsvp_registration_start_date = ( get_post_meta( get_the_ID(), 'asl_rsvp_registration_start_date', true ) ? get_post_meta( get_the_ID(), 'asl_rsvp_registration_start_date', true ) : false  );
        $asl_rsvp_registration_end_date = ( get_post_meta( get_the_ID(), 'asl_rsvp_registration_end_date', true ) ? get_post_meta( get_the_ID(), 'asl_rsvp_registration_end_date', true ) : false  );
        $asl_rsvp_registration_open = ( $asl_rsvp_registration_end_date ? ( strtotime( 'now' ) < date( strtotime( $asl_rsvp_registration_end_date ) ) ? true : false ) : true );
        $asl_rsvp_library_card_required = ( get_post_meta( get_the_ID(), 'library_card_required', true ) == true ? true : false );

        if ( strtotime( $end ) <= strtotime( 'now' ) ) :
        $asl_rsvp = false;
        endif;

        $is_user_logged_in = ( isset( $_SESSION[ 'auth' ][ 'user' ] ) ? true : false );
     ?>

<div id="content">

    <article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/Event">

        <header class="background-base event__header has-background hero">

            <div class="clearfix wrap">

                <div class="col-lg--eightcol col--centered">
                    <h1 class="beta event__header__title" itemprop="name"><?php the_title(); ?></h1>

                    <?php if ( has_excerpt() ) : ?>
                    <p itemprop="description"><?php echo get_the_excerpt(); ?></p>
                    <?php endif; ?>

                    <table class="table no-margin">
                        <tbody>
                            <tr>
                                <td>When</td>
                                <td>

                                <?php if ( !$allday && !$multiday ) : ?>

                                    <time itemprop="startDate"><?php echo date( 'l, F jS', strtotime( $start ) ); echo ( date( 'Y' ) == date( 'Y', strtotime( $start ) ) ? '' : date( ', Y ', strtotime( $start ) ) ); ?> &mdash; <?php echo date( 'g:i a', strtotime( $start ) ); ?></time> to
                                    <time itemprop="endDate"><?php echo date( 'g:i a', strtotime( $end ) ); ?></time>

                                <?php elseif ( $multiday && !$allday ) : ?>

                                    <time itemprop="startDate"><?php echo date( 'l, F jS', strtotime( $start ) ); echo ( date( 'Y' ) == date( 'Y', strtotime( $start ) ) ? '' : date( ', Y ', strtotime( $start ) ) );?> &mdash; <?php echo date( 'g:i a', strtotime( $start ) ); ?></time> &mdash;
                                    <time itemprop="endDate"><?php echo date( 'l, F jS', strtotime( $end ) ); ?>, <?php echo date( 'g:i a', strtotime( $end ) ); ?></time>

                                <?php elseif ( $multiday && $allday) : ?>

                                    <time itemprop="startDate"><?php echo date( 'l, F jS', strtotime( $start ) ); echo ( date( 'Y' ) == date( 'Y', strtotime( $start ) ) ? '' : date( ', Y ', strtotime( $start ) ) ); ?></time> &mdash;
                                    <time itemprop="endDate"><?php echo date( 'l, F jS', strtotime( $end ) ); ?></time>

                                <?php elseif ( $allday && !$multiday ) : ?>

                                    <time itemprop="startDate"><?php echo date( 'l, F jS', strtotime( $start ) ); echo ( date( 'Y' ) == date( 'Y', strtotime( $start ) ) ? '' : date( ', Y ', strtotime( $start ) ) ); ?></time>

                                <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td>Where</td>
                                <td><?php echo ( $location->description ? '<a href="#about-the-location">' : '' ) ?><?php echo $location->name; ?> <?php echo ( $location->description ? '<svg class="svg svg--search" viewBox="0 0 32 32"><use xlink:href="#icon-question"></use></svg></a>' : '' ) ?></td>
                            </tr>

                        </tbody>
                    </table>

                </div>

            </div><!--/.clearfix .wrap-->

        </header><!--/.background-base .event-_header .has-background .hero-->

        <div class="clearfix hero wrap">

            <section class="col-lg--eightcol <?php echo ( $asl_rsvp ? '' : 'col--centered' ) ?>" itemprop="articleBody">

                <?php the_content(); ?>

                <?php echo ( $call_to_action ? '<a class="button button--default button--small" href="' . $call_to_action_link . '">' . $call_to_action . '</a>': '' ); ?>

                <?php echo get_the_term_list( $post->ID, 'series', '<p>This event is part of the <span itemprop="superEvent">', ', ', '</span> series.</p>'); ?>

            </section> <!-- end article section -->

            <div class="<?php echo ( $asl_rsvp ? 'col-lg--fourcol has-cards' : 'col-lg--eightcol col--centered ' ) ?>" <?php echo ( $asl_rsvp ? 'style="padding: .5em;"' : '' ) ?>>

                <?php
                if ( $asl_rsvp ) :
                ?>
                    <section id="rsvp">

                        <?php
                        // If defined, the total number of seats available
                        $asl_rsvp_seats_capacity    = ( get_post_meta( get_the_ID(), 'asl_rsvp_seats_available', true ) ? get_post_meta( get_the_ID(), 'asl_rsvp_seats_available', true ) : false );

                        // The present number of seats reserved
                        $asl_rsvp_seats_reserved    = get_post_meta( get_the_ID(), 'asl_rsvp_registered_person', true );

                        /**
                         * If there is a set capacity, find the number of
                         * available seats remaining
                         */
                        if ( $asl_rsvp_seats_capacity ) :
                            $asl_rsvp_seats_available   = $asl_rsvp_seats_capacity - $asl_rsvp_seats_reserved;
                        endif; ?>


                        <?php if ( isset( $_COOKIE[ 'patron_has_posted'] ) ) : ?>
                        <p class="alert alert--success zeta">
                            <strong>Great!</strong> Thanks for signing up.
                        </p>
                        <?php endif; ?>

                        <header>
                            <h3><?php echo ( !isset( $_COOKIE[ 'patron_has_posted'] ) || !$is_user_logged_in ? 'Register' : 'Bring a guest!' ); ?></h3>
                            <?php if ( $is_user_logged_in && isset( $_COOKIE[ 'patron_has_posted'] ) ) : ?>
                            <p class="zeta">
                                Guests you register are associated with <em>your</em>
                                contact information, so you will be emailed all receipts.

                                <?php if ( $asl_rsvp_library_card_required && $is_user_logged_in ) : ?>
                                    Or, you can <a class="link" href="http://sherman.library.nova.edu/sites/spotlight/event/seussfest/logout.php">log out</a>
                                    and have them sign up individually.
                                <?php endif; ?>

                            </p>
                            <?php endif; ?>
                        </header>

                        <p>
                            <?php if ( $asl_rsvp_seats_capacity) : ?>
                            There <?php echo ( $asl_rsvp_seats_available > 1 ? 'are ' . $asl_rsvp_seats_available . ' spots': 'is just ' . $asl_rsvp_seats_available . ' spot' ); ?>
                            available out of <?php echo $asl_rsvp_seats_capacity; ?>.
                            <?php endif; ?>

                            <?php if ( $asl_rsvp_registration_end_date || $asl_rsvp_registration_start_date ) : ?>
                            Registration <?php echo ( $asl_rsvp_registration_start_date ? 'opens ' . date( 'l, F jS', strtotime( $asl_rsvp_registration_start_date) ) . ( $asl_rsvp_registration_end_date ? ' and ' : '' ) : '' );  ?> <?php echo ( $asl_rsvp_registration_end_date ? 'closes ' . date( 'l, F jS', strtotime( $asl_rsvp_registration_end_date) ) : '' ); ?>.
                            <?php endif; ?>

                        </p>

                        <?php
                        if ( $asl_rsvp_registration_open && ( !$asl_rsvp_registration_start_date || date( strtotime( $asl_rsvp_registration_start_date ) ) <= strtotime( 'now' ) ) ) :

                            // If no capacity is set or there are still seats available
                            if ( ! $asl_rsvp_seats_capacity || $asl_rsvp_seats_available > 0  ) :

                                $asl_rsvp_form_field_options = get_post_meta( get_the_ID(), 'asl_rsvp_form_field_options', true );
                                if ( !$asl_rsvp_form_field_options ) {
                                    $asl_rsvp_form_field_options = array();
                                }

                            ?>

                        <form id="registration-form" class="form" method="post" enctype="multipart/form-data" role="form" action="#">

                            <input type="hidden" name="post_id" value="<?php the_ID(); ?>" />
                            <?php wp_nonce_field( 'update_post_'. get_the_ID(), 'update_post_nonce' ); ?>

                            <?php if ( !$is_user_logged_in && $asl_rsvp_library_card_required ) : ?>

                            <div class="card" style="box-shadow: none;">
                                <p class="zeta">
                                    We require either an <a href="http://public.library.nova.edu/card" class="link">Alvin Sherman Library Card</a>
                                    or NSU SharkCard to register for this event.
                                </p>
                                <div class="align-right">
                                    <a href="https://sherman.library.nova.edu/auth/index.php?sites_url=<?php echo get_permalink(); ?>#rsvp" class="button button--small button--flat button--primary no-margin">Log In</a><br>
                                    or <a href="//public.library.nova.edu/card/" class="link link--undecorated small-text" target="new">Get a Card</a>
                                </div>
                            </div>

                            <small>
                                <b>Alvin Sherman Library Cards</b> are free! If you are already a Broward
                                County Library Cardholder you can <a href="http://lib.nova.edu/ecard" target="new">get an <b>e-card</b></a>
                                right now with no wait. Otherwise, <a href="http://public.library.nova.edu/card" target="new">fill out an application</a>.
                            </small>

                            <?php else : ?>
                            <ul class="zeta">

                                <li class="form__field">
                                    <label class="form__label">First Name</label>
                                    <input class="form__input form__input--full-width" type="text" id="first_name" name="first_name" placeholder="Jane" value="<?php echo ( $is_user_logged_in && !isset( $_COOKIE[ 'patron_has_posted'] ) ? $_SESSION[ 'auth' ][ 'user' ][ 'firstname' ] : '' ); ?>" required>
                                </li>

                                <li class="form__field">
                                    <label class="form__label">Last Name</label>
                                    <input class="form__input form__input--full-width" type="text" id="last_name" name="last_name" placeholder="Doe" value="<?php echo ( $is_user_logged_in && !isset( $_COOKIE[ 'patron_has_posted'] ) ? $_SESSION[ 'auth' ][ 'user' ][ 'lastname' ] : '' ); ?>"required>
                                </li>

                                <li class="form__field <?php echo ( $is_user_logged_in && isset( $_COOKIE[ 'patron_has_posted'] ) ? 'hide-accessible' : '' ); ?>">

                                    <label class="form__label">Email Address</label>
                                    <input class="form__input form__input--full-width" type="email" id="email_address" name="email_address" placeholder="hgranger@nova.edu" value="<?php echo ( $is_user_logged_in ? $_SESSION[ 'auth' ][ 'user' ][ 'email' ] : '' ); ?>" required>
                                </li>

                                <?php if ( in_array( "phone", $asl_rsvp_form_field_options ) ) : ?>
                                <li class="form__field <?php echo ( $is_user_logged_in && isset( $_COOKIE[ 'patron_has_posted'] ) ? 'hide-accessible' : '' ); ?>">
                                    <label class="form__label">Phone number</label>
                                    <input class="form__input form__input--full-width" type="tel" id="phone_number" name="phone_number" value="<?php echo ( $is_user_logged_in ? $_SESSION[ 'auth' ][ 'user' ][ 'telephone' ] : '' ); ?>" required>
                                </li>
                                <?php endif; ?>

                                <?php if ( in_array( "zip", $asl_rsvp_form_field_options ) ) : ?>
                                <li class="form__field">
                                    <label class="form__label">Zip code</label>
                                    <input class="form__input form__input--full-width" type="text" <?php //pattern="[0-9]+([-\,][0-9]+)?"?> id="zip" name="zip" value="<?php echo ( $is_user_logged_in ? $_SESSION[ 'auth' ][ 'user' ][ 'zip' ] : '' ); ?>">
                                </li>
                                <?php endif; ?>

                                <?php if ( in_array( "registrant_age", $asl_rsvp_form_field_options ) ) : ?>
                                <li class="form__field">
                                    <label class="form__label">Age</label>
                                    <input class="form__input form__input--full-width" type="number" pattern="[0-9]*" id="age" name="age" required>
                                </li>
                                <?php endif; ?>

                            <?php if ( $asl_rsvp_library_card_required ) : ?>

                                <?php if ( $is_user_logged_in ) : ?>

                                <input type="hidden" name="shark_id" id="shark_id" value="<?php echo ( $is_user_logged_in ? $_SESSION[ 'auth' ][ 'user' ][ 'univ_id' ] : '' ); ?>">

                                <li class="form__field">
                                    <label class="form__label">Library Card Number</label>
                                    <input type="text" class="form__input form__input--full-width" id="library_card" name="library_card" value="<?php echo ( $is_user_logged_in ? $_SESSION[ 'auth' ][ 'user' ][ 'barcode' ] : '' ); ?>" required>
                                    <small>
                                        <b>Alvin Sherman Library Cards</b> are free! If you are already a Broward
                                        County Library Cardholder you can <a href="http://lib.nova.edu/ecard" target="new">get an <b>e-card</b></a>
                                        right now with no wait. Otherwise, <a href="http://public.library.nova.edu/card" target="new">fill out an application</a>.
                                    </small>
                                </li>
                                <?php endif; ?>

                            <?php endif; ?>

                                <input class="button button--small button--primary--alt" type="submit" id="submit" value="Register" />

                                <?php if ( current_user_can( 'edit_posts' ) ) : ?>
                                <a href="#registered" class="button button--small peach">Registrants</a>
                                <?php endif; ?>

                            </ul>
                            <?php endif; ?>

                        </form>

                        <?php endif; // there are seats available ?>
                        <?php endif; // if registration is open ?>
                    <?php endif; ?>

                    </section><!--/#rsvp-->


                    <?php if ( current_user_can( 'edit_posts' ) ) : ?>
                    <section class="modal semantic-content" id="registered" tabindex="-1" role="dialog" aria-labelledby="modal-label" aria-hidden="true">

                        <div class="modal-inner">

                            <header id="modal-label">
                                <h3>Sign-up Form</h3>
                            </header>

                            <div class="modal-content clearfix">
                                <table class="table table" id="registrants">
                                    <thead>
                                        <tr class="has-background background-base">
                                            <th>First Name</th>
                                            <th>Last Name</th>
                                            <th>Email</th>

                                            <?php if ( in_array( "phone", $asl_rsvp_form_field_options ) ) : ?>
                                            <th>Phone</th>
                                            <?php endif; ?>

                                            <?php if ( in_array( "zip", $asl_rsvp_form_field_options ) ) : ?>
                                            <th>Zip</th>
                                            <?php endif; ?>

                                            <?php if ( in_array( "registrant_age", $asl_rsvp_form_field_options ) ) : ?>
                                            <th>Age</th>
                                            <?php endif; ?>

                                            <?php if ( $asl_rsvp_library_card_required ) : ?>
                                            <th>Card No.</th>
                                            <?php endif; ?>

                                            <?php if ( $asl_rsvp_library_card_required ) : ?>
                                            <th>Shark ID</th>
                                            <?php endif; ?>


                                        </tr>
                                    </thead>
                                    <tbody>
                                <?php if ( have_rows( 'asl_rsvp_registered_person' ) ) :            while ( have_rows( 'asl_rsvp_registered_person' ) ) : the_row(); ?>
                                        <tr>
                                            <td><?php the_sub_field( 'first_name' ); ?></td>
                                            <td><?php the_sub_field( 'last_name' ); ?></td>
                                            <td><?php the_sub_field( 'email_address' ); ?></td>

                                            <?php if ( in_array( "phone", $asl_rsvp_form_field_options ) ) : ?>
                                            <td><?php the_sub_field( 'phone_number' ); ?></td>
                                            <?php endif; ?>

                                            <?php if ( in_array( "zip", $asl_rsvp_form_field_options ) ) : ?>
                                            <td><?php the_sub_field( 'zip' ); ?></td>
                                            <?php endif; ?>

                                            <?php if ( in_array( "registrant_age", $asl_rsvp_form_field_options ) ) : ?>
                                            <td><?php the_sub_field( 'age' ); ?></td>
                                            <?php endif; ?>

                                            <?php if ( $asl_rsvp_library_card_required ) : ?>
                                            <td><?php the_sub_field( 'library_card'); ?></td>
                                            <?php endif; ?>

                                            <?php if ( $asl_rsvp_library_card_required ) : ?>
                                            <td><?php the_sub_field( 'shark_id'); ?></td>
                                            <?php endif; ?>
                                        </tr>


                                <?php endwhile; endif; ?>
                                    </tbody>
                                </table>
                            </div>

                            <footer class="align-right clearfix no-margin zeta">
                                <a class="button button--default button--small" href="#!" data-close="Close" data-dismiss="modal">Close</a>
                                <a class="button button--small button--primary--alt js-csv" href="#" download="test.csv">Download</a>
                                <script>

                                    var patronsTable = document.querySelector( '#registrants' ),
                                        rowsLength = patronsTable.rows.length,
                                        colLength = patronsTable.rows[0].cells.length,
                                        tableString = "";

                                    for ( var i = 0; i < colLength; i++ ) {
                                        tableString += patronsTable.rows[0].cells[i].innerHTML.split(",").join("") + ",";
                                    }

                                    tableString = tableString.substring( 0 , tableString.length - 1 );
                                    tableString += "\r\n";

                                    for ( var j = 1; j < rowsLength; j++ ) {
                                        for ( var k = 0; k < colLength; k++ ) {
                                            tableString += patronsTable.rows[j].cells[k].innerHTML.split(",").join("") + ",";
                                        }
                                        tableString += "\r\n";
                                    }

                                    document.querySelector( 'a.js-csv' ).addEventListener( 'click', function( e ) {

                                        this.href = 'data:application/csv;charset=utf-8,' + encodeURIComponent( tableString );
                                        this.download = 'event.csv';
                                    });


                                </script>
                            </footer>
                        </div>

                        <a href="#!" class="modal-close" title="Close this modal" data-close="Close" data-dismiss="modal">×</a>

                    </section>
                    <?php endif; ?>

                    <?php if ( $contact_info ) : ?>
                    <div class="hero--small">
                        <h4 class="delta" id="contact">Any questions?</h4>
                        <?php echo '<p class="zeta no-margin">' . get_post_meta( get_the_ID(), 'other_contact_instructions', true ) . '</p>'; ?>
                    </div>
                    <?php endif; ?>

                </div><!--/.col-md fourcol-->

</div><!--/.clearfix .wrap-->


</article> <!-- end article -->

    <?php endwhile; ?>

    <?php else : ?>

    <article id="post-not-found" class="hentry clearfix">
        <header class="article-header">
            <h1><?php _e("Oops, Post Not Found!", "bonestheme"); ?></h1>
        </header>
        <section class="post-content">
            <p><?php _e("Uh Oh. Something is missing. Try double checking things.", "bonestheme"); ?></p>
        </section>
        <footer class="article-footer">
            <p><?php _e("This is the error message in the page.php template.", "bonestheme"); ?></p>
        </footer>
    </article>

<?php endif; ?>

</div> <!-- end #content -->

<?php if ( $location->description ) :  ?>
<section class="modal semantic-content" id="about-the-location" tabindex="-1" role="dialog" aria-labelledby="modal-label" aria-hidden="true">

    <div class="modal-inner">

        <header id="modal-label">
            <h3><?php echo $location->name; ?></h3>
        </header>

        <div class="modal-content clearfix">
            <p>
                <?php echo $location->description; ?>
            </p>

            <a class="button button--link small-text" href="#">Close</a>
        </div>

    </div>

    <a href="#!" class="modal-close" title="Close this modal" data-close="Close" data-dismiss="modal">×</a>

</section>
<?php endif; ?>

<?php get_footer(); ?>
