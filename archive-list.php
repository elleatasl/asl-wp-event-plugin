<?php get_header(); ?>

<style>
.list--books {
  list-style-type: none;
  padding: 0;
}
.list--books:hover li {
  opacity: .9;
  -webkit-transition: all .2s ease-out;
          transition: all .2s ease-out;
}
.list--books li {
  background-color: white !important;
  padding: 0.35%;
  border: 0;
  box-shadow: none;
  margin-top: 0;
}
.list--books li:hover {
  opacity: 1;
}
.list--books{
  overflow: hidden;
  position: absolute;
  left:0;
  width: 100%;
  top: 0;
  vertical-align: bottom;
}
.list--books__cover {
  background-color: #f5f5f5;
  display: block;
  height: 10.333rem;
  position: relative;
  overflow: hidden;
}
.list--books__cover > img {
  bottom: 0;
  box-shadow: 0px 5px 11px -3px rgba(0, 0, 0, 0.3);
  vertical-align: top;
}

</style>

	<div id="content" class="has-cards">

		<header class="clearfix has-background background-base">

				<div class="col-md--eightcol col--centered hero">

						<h1 class="hide-accessible">Book Lists</h1>
					<p class="no-margin">
						We make lists to make it easier for you to find something to read. They're
						hand-picked by the sort of people who know their books.
					</p>
				</div>

		</header>


		<div id="inner-content" class="wrap clearfix">

		    <main role="main" class="clearfix js-masonry">

      <?php

        $paged = ( get_query_var( 'paged') ? get_query_var( 'paged' ) : '' );
        $s = ( get_query_var( 'search' ) ? get_query_var( 'search' ) : '' );
        $for = get_query_var( 'for', array( 'kids', 'teens', 'public', 'academic' ) );
        $args = array(
          'post_type' => 'list',
          'tax_query' => array(
            array(
              'taxonomy' => 'library-audience',
              'field' => 'slug',
              'terms' => $for
            )
          ),
          'posts_per_page' => 20,
          'paged' => $paged,
          's' => ( $s ? $s : null )
        );

        $the_query = new WP_Query( $args );

        if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
        <?php $count++; ?>
        <?php //if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

				<div class="col-md--sixcol col-lg--fourcol clearfix">
          <a href="<?php the_permalink(); ?>" class="link link--undecorated">
            <article class="card">
              <?php if ( has_post_thumbnail() ) : ?>
                <div class="card__media">
                  <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() ) ); ?>" alt="<?php the_title(); ?>">
                </div>
              <?php endif;?>
              <header class="card__header">
                <span class="card__color-code <?php echo ( has_term( 'teens', 'library-audience' ) ? 'card__color-code--teens' : ( has_term( 'kids', 'library-audience' ) ? 'card__color-code--kids' : '' ) )?>"></span>
                <h2 class="card__title delta <?php echo ( !has_post_thumbnail() ? 'no-margin' : '' ); ?>"><?php the_title(); ?></h2>
              </header>
              <section class="card__content">
                <p class="zeta"><?php echo get_the_excerpt(); ?></p>
              </section>
            </article>
          </a>

				</div>
		    <?php endwhile; ?>
        <?php wp_reset_postdata(); ?>

				</main> <!-- end #main -->
        <nav class="align-center pagination">
        	<?php previous_posts_link( 'Previous' ); ?><?php ( $count === 0 || $count < 20 ? '' : ( $paged < 2  ? next_posts_link( 'More Lists') : next_posts_link( 'Even More Lists!' ) ) ); ?>
        </nav>
			    <?php else : ?>

				    <article id="post-not-found" class="hentry clearfix">
					    <header class="article-header">
						    <h1><?php _e("Oops, Post Not Found!", "bonestheme"); ?></h1>
				    	</header>
					    <section class="post-content">
						    <p><?php _e("Uh Oh. Something is missing. Try double checking things.", "bonestheme"); ?></p>
						</section>
    					<footer class="article-footer">
	    				    <p><?php _e("This is the error message in the archive.php template.", "bonestheme"); ?></p>
		    			</footer>
			    	</article>

			    <?php endif; ?>

        </div> <!-- end #inner-content -->

	</div> <!-- end #content -->


<?php get_footer(); ?>
