<?php get_header(); ?>
	
	<header class="hero has-background background-base">
		<h1 class="align-center title">Location: <?php single_cat_title(); ?></h1>
		<div class="eightcol center-grid"><?php echo term_description(); ?></div>
	</header>		


			<div id="content">
			
				<div id="inner-content" class="clearfix">
				
				    <main id="main" class="eightcol hero center-grid clearfix" role="main">

 			<?php 
 			$qobj = get_queried_object();

 			$args = array(
 				'meta_key' 	=> 'event_start',
 				'order'		=> 'ASC',
 				'orderby'	=> 'meta_value_num',
 				'post_type' => 'spotlight_events',
 				'location' => $qobj->slug
 			);

		 	$featured = new WP_Query ( $args ); 

			if ( $featured->have_posts() ) : while ( $featured->have_posts() ) : $featured->the_post();
			
			/* ==================
			 *  Layout Options
			 */ $title 					= get_post_meta( get_the_ID(), 'overlay_title', true );

				if ( !$title ) {
					$title = get_the_title();
				}

			/* ==================
			 * Date Options 
			 */	$allday					= false;
				$multiday				= false;				
				$date_start				= get_post_meta( get_the_ID(), 'event_start', true );
				$date_options			= get_post_meta( get_the_ID(), 'scheduling_options', true );
				$start = '';
				$end = '';

				if ( !$date_options ) {
					$event_start_time = get_post_meta( get_the_ID(), 'event_start_time', true );
					$event_end_time = get_post_meta( get_the_ID(), 'event_end_time', true );
					$start = $date_start . $event_start_time;
					$end 	= $date_start . $event_end_time;

				} else {

					if ( in_array( "multiday", $date_options ) ) {

						$start = $date_start;					
						$multiday = true;

						if ( in_array( "allday", $date_options ) ) {

							$allday = true;
							$end = get_post_meta( get_the_ID(), 'event_end', true );						

						} else {

							$end = get_post_meta( get_the_ID(), 'event_end', true ) . get_post_meta( get_the_ID(), 'event_end_time', true );
						}

					}

				}
			?>

			<?php if ( strtotime($end) > time() ) : ?>
			
			<article id="post-<?php the_ID(); ?>" <?php post_class('post clearfix'); ?> itemscope itemtype="http://schema.org/Event" role="article">				

				<div class="threecol first">
					<time class="time" datetime="<?php echo $start . ( $multiday === true ? '-' . $end : ''); ?>">
						<span itemprop="startDate" content="<?php echo $start ?>"><?php echo date('F jS', strtotime($start) ); ?></span> <?php echo ( $multiday === false ? '' : '<span class="zeta">through <span itemprop="endDate" content="' . $end . '">' . date('F jS', strtotime($end) ) . '</span></span>' ); ?>
						
						<?php if ( !$allday || !$multiday ) : ?>
						<span class="small-text time__hours">					
							<?php echo date( 'g:i a', strtotime( $start ) ); ?> - <?php echo date('g:i a', strtotime( $end )); ?>						
						</span>
						<?php endif; ?>

					</time>
				</div>

				<div class="ninecol last">
					<header class="event__header">
						<?php echo get_the_term_list( $post->ID, 'series', '<span itemprop="superEvent"><b>', ', ', '</b></span>'); ?>					
						<h2 class="delta event__header__title"><a href="<?php the_permalink(); ?>" itemprop="url"><?php echo $title; ?></a></h2>				
						
					</header>
					<p class="zeta no-margin"> <?php echo ( has_excerpt() ? get_the_excerpt() : '' ); ?> <?php echo get_the_term_list( $post->ID, 'event_type', ' | ', ', '); ?> </p>
				</div>
					

			</article>
			<?php endif; ?>
		
		    <?php endwhile; ?>	
            <nav class="wp-prev-next">
		        <ul class="no-bullets clearfix">
			        <li class="prev-link"><?php next_posts_link(_e('&laquo; Older Entries', "bonestheme")) ?></li>
			        <li class="next-link"><?php previous_posts_link(_e('Newer Entries &raquo;', "bonestheme")) ?></li>
		        </ul>
    	    </nav>
		    <?php endif; ?>

			
    				</main> <!-- end #main -->
                    
                </div> <!-- end #inner-content -->
                
			</div> <!-- end #content -->

<?php get_footer(); ?>