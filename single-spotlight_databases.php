<?php get_header(); ?>
<svg display="none" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="32" height="32" viewBox="0 0 32 32">
	<symbol id="icon-apple"  viewBox="0 0 1024 1024">
		<title>apple</title>
		<path class="path1" d="M791.498 544.092c-1.294-129.682 105.758-191.876 110.542-194.966-60.152-88.020-153.85-100.078-187.242-101.472-79.742-8.074-155.596 46.948-196.066 46.948-40.368 0-102.818-45.754-168.952-44.552-86.916 1.292-167.058 50.538-211.812 128.38-90.304 156.698-23.126 388.84 64.89 515.926 43.008 62.204 94.292 132.076 161.626 129.58 64.842-2.588 89.362-41.958 167.756-41.958s100.428 41.958 169.050 40.67c69.774-1.296 113.982-63.398 156.692-125.796 49.39-72.168 69.726-142.038 70.924-145.626-1.548-0.706-136.060-52.236-137.408-207.134zM662.562 163.522c35.738-43.358 59.86-103.512 53.28-163.522-51.478 2.096-113.878 34.29-150.81 77.55-33.142 38.376-62.148 99.626-54.374 158.436 57.466 4.484 116.128-29.204 151.904-72.464z"></path>
	</symbol>

	<symbol id="icon-android"  viewBox="0 0 1024 1024">
		<title>android</title>
		<path class="path1" d="M864 320c-35.2 0-64 28.8-64 64v256c0 35.2 28.8 64 64 64s64-28.8 64-64v-256c0-35.2-28.8-64-64-64zM96 320c-35.2 0-64 28.8-64 64v256c0 35.2 28.8 64 64 64s64-28.8 64-64v-256c0-35.2-28.802-64-64-64zM192 736c0 53.020 42.98 96 96 96h32v128c0 35.2 28.8 64 64 64s64-28.8 64-64v-128h64v128c0 35.2 28.802 64 64 64s64-28.8 64-64v-128h32c53.020 0 96-42.98 96-96v-352h-576v352zM598.598 89.494l40.504-77.806c2.032-3.902 0.5-8.758-3.402-10.79s-8.758-0.5-10.79 3.402l-41.118 78.986c-32.198-12.448-67.194-19.286-103.792-19.286-36.596 0-71.592 6.838-103.796 19.286l-41.116-78.984c-2.032-3.902-6.886-5.434-10.79-3.402s-5.434 6.886-3.402 10.79l40.5 77.808c-90.63 41.018-156.24 127.584-167.62 230.504h572.44c-11.376-102.924-76.986-189.488-167.618-230.508zM352 270.4c-25.626 0-46.4-20.774-46.4-46.4s20.774-46.4 46.4-46.4 46.4 20.774 46.4 46.4c-0.002 25.626-20.774 46.4-46.4 46.4zM608 270.4c-25.626 0-46.402-20.774-46.402-46.4s20.778-46.4 46.402-46.4 46.398 20.774 46.398 46.4c0 25.626-20.772 46.4-46.398 46.4z"></path>
	</symbol>
</svg>

<style type="text/css">

.svg--apple, .svg--android {
	float: left;
	margin-top: .5em;
}

.svg--android {
	fill: #A4C639;	
}
.svg--android:active,
.svg--android:hover {
	fill: #BAE23D;
}

.svg--apple {
	fill: #222;
}

.svg--apple:active,
.svg--apple:hover {
	fill: #444;
}

@media only screen 
and (min-width : 37.375em)  {

	.card--callout {
		border-top: 1px solid #e9e9e9;
		border-bottom: 1px solid #e9e9e9;
		margin: 1em 0;
		padding: 1em 0;
	}

}

@media only screen 
and (min-width : 64em)  {
}
</style>

	<div id="content">
			
		    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		    <?php 
		    	$authURL		= get_post_meta(get_the_ID(), 'authenticated_url', true);
		    	$dbID			= get_post_meta(get_the_ID(), 'aid', true);
		    	$resourceLogo 	= get_post_meta( get_the_ID(), 'resource_logo', true );
		    	$instructions = get_post_meta(get_the_ID(), 'instructions', true);
		    	$tutorial = get_post_meta(get_the_ID(), 'handout', true);
		    	$screencast = get_post_meta(get_the_ID(), 'screencast', true);
		    	$apple_link = get_post_meta( get_the_ID(), 'apple_link', true );
		    	$google_play_link = get_post_meta( get_the_ID(), 'google_play_link', true );
		    	$kindle_link = get_post_meta( get_the_ID(), 'kindle_link', true );

		    	$screencast = substr( $screencast, strpos($screencast, 'watch/') + 6 ); // knowing "watch/"
			?>

			<header>
				<h1 class="hide-accessible"><?php the_title(); ?></h1>
			</header>

			<!-- Main Article
			======================
			--> <article id="post-<?php the_ID(); ?>" <?php post_class('col-md--eightcol col--centered clearfix'); ?> role="article">

					<div class="col-md--tencol col--centered  clearfix hero media">

						<?php if ( has_post_thumbnaiL() ) : ?>
							<?php the_post_thumbnail( 'media-large', array( 'itemprop' => 'image' ) ); ?>
						<?php endif; ?>		

						<?php if ( has_excerpt() ): ?>
							<p class="card--callout zeta">
								<?php echo get_the_excerpt(); ?>
							</p>
						<?php endif ?>

									
						<nav role="navigation">

							<?php if ( get_post_meta( get_the_ID(), 'has_app', true ) ) : ?>	
						 		
								<?php if ( $apple_link ) : ?>
									<a href="<?php echo $apple_link; ?>" title="iOS Flipster App">
										<svg class="svg svg--apple" viewBox="0 0 32 32" title="iOS"><use xlink:href="#icon-apple"></use></svg>
										<span class="hide-accessible">Download the iOS App</span>
									</a>
								<?php endif; ?>
						 		
						 		<?php if ( $google_play_link ) : ?>
									<a class="link--app-store" href="<?php echo $google_play_link; ?>" title="Google Play Flipster App">
										<svg class="svg svg--android" viewBox="0 0 32 32" title="Android"><use xlink:href="#icon-android"></use></svg>
										<span class="hide-accessible">Download the Android App</span>
									</a>
								<?php endif; ?>
						 		
							<?php endif; ?>

							<div class="align-right">								
							<?php if ( $tutorial = '' ) : ?>
					       	<a class="button button--small button--default" href="<?php echo $tutorial; ?>" title="Tutorial">
					    		Help Guide
					    	</a>					    
						<?php endif; ?>
							<a class="button button--link small-text" href="//public.library.nova.edu/card/">Get a Library Card</a> &nbsp;
							<a class="button button--small button--default" href="<?php echo $authURL ?>" title="<?php the_title(); ?>">Log In</a> 

							</div>
							
						</nav>

					</div>
						
				<section class="post-content">
			    	<?php the_content(); ?>					

					<?php if ( $instructions != '' ) : ?>
					<h3>Getting Started</h3>
					<p><?php echo $instructions; ?></p>
					<?php endif; ?>

			    </section> <!-- end article section -->

			    </article> <!-- end article -->
					
			    <?php endwhile; ?>			
			
			    <?php else : ?>
			
				<article id="post-not-found" class="hentry clearfix">
					<header class="article-header">
						<h1><?php _e("Oops, Post Not Found!", "bonestheme"); ?></h1>
					</header>
					<section class="post-content">
						<p><?php _e("Uh Oh. Something is missing. Try double checking things.", "bonestheme"); ?></p>
					</section>
					<footer class="article-footer">
					    <p><?php _e("This is the error message in the single-custom_type.php template.", "bonestheme"); ?></p>
					</footer>
					</article>
			
			    <?php endif; ?>
    
	</div> <!-- end #content -->

<?php get_footer(); ?>